﻿#SingleInstance force

SetWorkingDir %A_ScriptDir%
PROGRAMFILESX86 := A_ProgramFiles . (A_PtrSize=8 ? " (x86)" : "")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SYSTEM COMMANDS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

; Minimize window (Alt+Z)
LAlt & z::
WinMinimize A
Return

; Close window (Alt+X)
LAlt & x::
WinClose A
Return

; Max & Resore window (Alt+W)
!w::
WinGet, hWnd, ID, A
WinGet, vWinMinMax, MinMax, % "ahk_id " hWnd
if (vWinMinMax = 1)
    WinRestore, % "ahk_id " hWnd
else
    WinMaximize, % "ahk_id " hWnd
Return

; Sleep PC (Alt+F11)
#F11::
  DllCall("PowrProf\SetSuspendState", "int",s 0, "int", 0, "int", 0)
Return

; Shutdown PC (Alt+F12)
#F12::
  Shutdown, 1
Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; APPLICATIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

; Telegram (ALT+1)
LAlt & 1::
Run "%APPDATA%\Telegram Desktop\Telegram.exe"
Return

; eM Client (ALT+2)
LAlt & 2::
Run "%PROGRAMFILESX86%\eM Client\MailClient.exe"
return

; Skype-UWP (ALT+3)
LAlt & 3::
Run "Lnk\Skype.lnk"
return

; Slack-UWP (ALT+4) 
LAlt & 4::
Run "%LOCALAPPDATA%\slack\slack.exe"
Return

; Outlook (ALT+5) 
LAlt & 5::
Exe = "outlook.exe"
Path = "%PROGRAMFILESX86%\Microsoft Office\root\Office16\outlook.exe"
WinTitle = "Входящие - pobyrokhin.k@novapay.ua - Outlook"
ProcessExist(Exe,Path)
WinShowHide(WinTitle)
Return

; WinBox (ALT+0) 
LAlt & 0::
Run "%APPDATA%\Mikrotik\Winbox\WinBox.exe"
return

;  WindowsTerminal-UWP (ALT+F1)
LAlt & F1::
Run "Lnk\Terminal.lnk"
Return

; WindowsTerminal-UWP Admin (CTRL+F1)
LCtrl & F1::
Run *RunAs "Lnk\Terminal.lnk"
Return

; Notepad3 (ALT+F2)
LAlt & F2::
Run "%PROGRAMFILES%\Notepad3\Notepad3.exe"
return

; Notepad++  (ALT+F3)
LAlt & F3::
Run "%PROGRAMFILES%\Notepad++\notepad++.exe"
return

; MobaXterm (ALT+F5)
LAlt & F5::
Run "E:\Dropbox\DEV\WRK\MobaXterm\MobaXterm.exe" -i "E:\Dropbox\DEV\WRK\MobaXterm\Profile\MobaXterm.ini"
return

; PCRadio (ALT+F6)
LAlt & F6::
Exe = "pcradio.exe"
Path = "%PROGRAMFILESX86%\PCRadio\PCRadio.exe"
WinTitle = "PCRadio"
ProcessExist(Exe,Path)
WinShowHide(WinTitle)
Return

;; ALT + 1-9
/*

; Evernote
LAlt & F7::
Run "%PROGRAMFILESX86%\Evernote\Evernote\Evernote.exe"
;Exe = "evernote.exe"
;Path = "%PROGRAMFILESX86%\Evernote\Evernote\Evernote.exe"
;WinTitle = "Evernote"
;ProcessExist(Exe,Path)
;WinShowHide(WinTitle)
Return


; VS Code
LAlt & 4::
Run "%LOCALAPPDATA%\Programs\Microsoft VS Code\Code.exe"
return

; SnagitEditor
LAlt & 6::
Run "%PROGRAMFILES%\TechSmith\Snagit 2020\SnagitEditor.exe"
return

*/

; Copy, Cut, Paste, Save
/*
F1::
send, ^c
Return

F2::
send, ^x
Return

F3::
send, ^v
Return

F4::
send, ^s
Return
*/ 

; ##################################################################

; Func ProcessExist
ProcessExist(Exe,Path)
{
    StringReplace, Exe, Exe, ", , All    
    Process,Exist,%Exe%
    If !ErrorLevel
        run %Path%
}

; Func WinShowHide
WinShowHide(WinTitle)
{   
    StringReplace, WinTitle, WinTitle, ", , All    
    WinGet WinState, MinMax, %WinTitle%
        if (WinState = -1)
            WinRestore, %WinTitle%
        else
            WinMinimize, %WinTitle%
}

; Func MoveToSecMonitor
MoveToSecMonitor(WinTitle)
{
    StringReplace, WinTitle, WinTitle, ", , All    
    SysGet, Display_, Monitor, 1
    WinGetPos,,, WinW, WinH, % WinTitle
    WinMove, % WinTitle,, % Floor( (Display_Right/2)-(WinW/2)+1500 ), % Floor( (Display_Bottom/2)-(WinH/2)-200 )
}

; Func CenterWindow
CenterWindow(WinTitle)
{
    WinGetPos,,, Width, Height, %WinTitle%
    WinMove, %WinTitle%,, (A_ScreenWidth/2)-(Width/2), (A_ScreenHeight/2)-(Height/2)
}